<?php

/**
 * Implementation of hook_menu()
 */
function fast_register_menu($may_cache)
{
	global $user;
	$items = array();
	if ($may_cache)
	{
		$items[] = array(
			'path' => 'admin/settings/fast_register', 
			'title' => t('Fast Registration'),
      		'callback' => 'drupal_get_form', 
			'callback arguments' => array('fast_register_admin_settings'), 
			'access' => user_access('administer site configuration'), 
			'type' => MENU_NORMAL_ITEM
		);
		
		$items[] = array(
			'path' => 'user/register/fast', 
			'title' => t('Create new account'),
      		'callback' => 'drupal_get_form', 
			'callback arguments' => array('user_fast_register'), 
			'access' => !$user->uid && variable_get('user_register', 1), 
			'type' => MENU_NORMAL_ITEM
		);
	}
	else
	{
		if (arg(0) == 'user' && arg(1) == 'register' && arg(2) == 'full')
		{
			$items[] = array(
				'path' => 'user/register/full', 
				'title' => t('Fill full user profile'),
      			'callback' => 'drupal_get_form', 
				'callback arguments' => array('user_fast_register_full'), 
				'access' => $user->fast_register === 0, 
				'type' => MENU_CALLBACK
			);
		}
	}
	return $items;
}

/**
 * Form for edit full profile
 *
 * @return array
 */
function user_fast_register_full()
{
	global $user;
	$u = user_load(array('uid' => $user->uid));
	$form = _user_forms(get_object_vars($u), $u, NULL, 'register');
	$form['submit'] = array('#type' => 'submit', '#value' => t('Save'), '#weight' => 30);
	return $form;
}

/**
 * Validation of full profile edit form
 *
 * @param string $form_id
 * @param array $form_values
 */
function user_fast_register_full_validate($form_id, $form_values)
{
	foreach (module_list() as $module) {
		$function = $module .'_user';
		if ($module != 'user' && function_exists($function)) {
			$function('validate', $form_values, $form_values, 'account');
		}
	}
}

/**
 * Save full user profile
 *
 * @param string $form_id
 * @param array $form_values
 */
function user_fast_register_full_submit($form_id, $form_values)
{
	global $user;
	unset($user->fast_register);
	$form_values['fast_register'] = NULL;
	unset($form_values['op']);
	unset($form_values['submit']);
	unset($form_values['form_token']);
	unset($form_values['form_id']);
	$categories = _user_categories($account);
	foreach ($categories as $cat)
	{
		user_save($user, $form_values, $cat['name']);
	}
	user_module_invoke('after_full_registration', $form_values, $user, 'full_registration');
	return '';
}

/**
 * From for fast registration
 *
 * @return array
 */
function user_fast_register()
{
	$form = user_edit_form(NULL, NULL, TRUE);

	$null = NULL;
	$extra = _user_forms($null, NULL, 'fast_register', 'form');

	// Remove form_group around default fields if there are no other groups.
	if (!$extra) 
	{
		$form['name'] = $form['account']['name'];
		$form['mail'] = $form['account']['mail'];
		$form['pass'] = $form['account']['pass'];
		$form['status'] = $form['account']['status'];
		$form['roles'] = $form['account']['roles'];
		$form['notify'] = $form['account']['notify'];
		unset($form['account']);
	}
	else 
	{
		$form = array_merge($form, $extra);
	}
	
	$form['submit'] = array(
	'#type' => 'submit', 
	'#value' => t('Create new account'), 
	'#weight' => 30
	);

	return $form;
}


/**
 * Validation of fast register form
 *
 * @param string $form_id
 * @param array $form_values
 */
function user_fast_register_validate($form_id, $form_values)
{
	_user_edit_validate(arg(1), $form_values);
}

/**
 * Submit for fast register form. Register user and adds fast_register flag into user object
 *
 * @param string $form_id
 * @param array $form_values
 * @return string
 */
function user_fast_register_submit($form_id, $form_values)
{
	$allowed_pages_count = variable_get('fast_register_allowed_visits', 1);
	$form_values['fast_register'] = $allowed_pages_count;
	return user_register_submit($form_id, $form_values);
}

/**
 * Implementation of hook_init()
 * Checks if user was reagistered with fast register form,
 * redirects user to fill full profile form if count of allowed page visits = 0
 *
 */
function fast_register_init()
{
	global $user;
	$edit_path = 'user/register/full';
	$allowed_paths = array($edit_path, 'user/login', 'logout');

	if (isset($user->fast_register) &&  !in_array($_GET['q'], $allowed_paths))
	{
		if ($user->fast_register > 0)
		{
			user_save($user, array('fast_register' => $user->fast_register - 1));
		}
		else
		{
			drupal_goto($edit_path, drupal_get_destination());
		}
	}
}

/**
 * Implementation of hook_user()
 * Fixes integration with some other modules if they are installed
 */
function fast_register_user($op, &$edit, &$account, $category = NULL)
{
	switch ($op)
	{
		case 'form':
			if ($category == 'fast_register')
			{
				$form = array();
				if (module_exists('mollom') && variable_get('mollom_user_fast_register', MOLLOM_MODE_DISABLED))
				{
					$form['style'] = array(
					'#type' => 'markup',
					'#value' => '<style media="all" type="text/css">#captcha{visibility:hidden;}#captcha IMG{visibility:visible;}</style>'
					);
				}
				
				if (module_exists('autoassignrole'))
				{
					autoassignrole_form_alter('user_register', $form);
				}
				return $form;
			}
			break;
	}
}

/**
 * Implementation of hook_block().
 * Provides block with fast registration form
 */
function fast_register_block($op = 'list', $delta = 0, $edit = array())
{
	global $user;
	if ($op == 'list')
	{
		$blocks[0] = array('info' => t('Fast registration'));
		return $blocks;
	}
	elseif ($op == 'view' && $user->uid == 0)
	{
		switch ($delta) {
			case 0:
					$block['subject'] = t('Register');
					$block['content'] = drupal_get_form('user_fast_register');
				break;
		}
		return $block;
	}
}

/**
 * Admin settings form
 * @return array
 */
function fast_register_admin_settings()
{
	$form = array();
	$form['fast_register_allowed_visits'] = array(
	'#type' => 'textfield',
	'#title' => t('Count of pages user is allowed to visit after fast registration'),
	'#description' => t('After this count of visits user will be forced to fill in full profile'),
	'#default_value' => variable_get('fast_register_allowed_visits', 1)
	);
	return system_settings_form($form);
}


function fast_register_admin_settings_validate($form_id, &$form_values)
{
	if (intval($form_values['fast_register_allowed_visits']) < 1)
	{
		form_set_error('fast_register_allowed_visits', t('Count of pages can\'t be less than 1'));
	}
}
