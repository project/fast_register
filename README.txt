
Fast registration

The module provides a form for fast user registration with email and username only. 
Other modules also can add another fields on this form.
After fast registration user is allowed to visit a configurable count of pages, after which he must fill in full user profile. 

To install, place the entire module folder into your modules directory.
Go to administer -> site building -> modules and enable the Fast Registration module.

To change count of allowed page visits go to Administer -> Site configuration -> Fast Registration.

Fast registration form can be available as a block or at path /user/register/fast.

//TODO
1. Add settings for allowed paths
2. Add settings for profile fields, included in fast registration form